using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownPocket : MonoBehaviour
{
    public SO_Color color;
    private Image sprite;
    private float cooldown;
    void Awake()
    {
        sprite = GetComponent<Image>();
    }

    IEnumerator CooldownReduce()
    {
        sprite.fillAmount = 1;
        while (true)
        {
            cooldown += Time.deltaTime;
            sprite.fillAmount = 1 - cooldown / color.PocketCooldown;

            if (cooldown >= color.PocketCooldown)
            {
                sprite.fillAmount = 0;
                break;
            }
                
            yield return null;
        }
        
    }

    public void OnColorUsed()
    {
        cooldown = 0;
        StartCoroutine(CooldownReduce());
    }
}
