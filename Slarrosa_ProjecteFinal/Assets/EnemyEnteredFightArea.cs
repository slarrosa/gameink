using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEnteredFightArea : MonoBehaviour
{
    public SO_EnemyStats enemy;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            enemy.activated = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            enemy.activated = false;
        }
    }
}
