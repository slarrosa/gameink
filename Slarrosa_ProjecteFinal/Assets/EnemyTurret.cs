using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurret : MonoBehaviour
{
    public SO_EnemyStats stats;
    public GameObject bullet;
    private bool YposUp;
    private bool shoted = false;
    public SO_BulletInfo target;
    public SO_Color colors;
    private Color blueColor = new Color(0f, 0f, 1f, 1f);
    private Color greenColor = new Color(0f, 1f, 0f, 1f);
    private Color redColor = new Color(1f, 0f, 0f, 1f);

    Vector3 bulletStartPosition;
    private int random;
    private void Awake()
    {
        stats.trueDisabled = false;
        stats.disabled = false;
        random = Random.Range(0, 3);
        if (random == 0)
            this.gameObject.GetComponent<SpriteRenderer>().color = greenColor;
        if (random == 1)
            this.gameObject.GetComponent<SpriteRenderer>().color = blueColor;
        if (random == 2)
            this.gameObject.GetComponent<SpriteRenderer>().color = redColor;
    }
    void Start()
    {
        stats.activated = false;
        if(this.transform.position.y > 0f)
        {
            //Si la torreta esta al sostre posem el boolea a true per despres calcular la posicio de la bala
            YposUp = true;
        }
        else
        {
            YposUp = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (colors.actualColor == greenColor && random == 0)
        {
            stats.disabled = true;
        }
        else if (colors.actualColor == blueColor && random == 1)
        {
            stats.disabled = true;
        }
        else if (colors.actualColor == redColor && random == 2)
        {
            stats.disabled = true;
        }
        else
        {
            stats.disabled = false;
        }
        if (stats.activated && !shoted && !stats.disabled && !stats.trueDisabled)
        {
            shoted = true;
            bulletStartPosition = this.transform.GetChild(0).position;
            StartCoroutine(Shot());
        }
        if(!YposUp)
            this.transform.up = target.playerPos-this.transform.position;
        else
            this.transform.up = -(target.playerPos-this.transform.position);
    }

    IEnumerator Shot()
    {
        Instantiate(bullet);
        bullet.transform.position = bulletStartPosition;
        yield return new WaitForSecondsRealtime(3f);
        shoted = false;
    }
}
