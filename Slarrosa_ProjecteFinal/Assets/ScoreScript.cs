using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreScript : MonoBehaviour
{
    private float score;
    private bool enterCorroutine = true;
    public SO_Score myscore;
    void Awake()
    {
        myscore.scoreStop = false;
        score = 500;
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Score: " + score;
        if (enterCorroutine && !myscore.scoreStop)
            StartCoroutine(lowerByTime());
        myscore.score = score;
    }

    IEnumerator lowerByTime()
    {
        enterCorroutine = false;
        yield return new WaitForSecondsRealtime(1f);
        score--;
        enterCorroutine = true;
    }
}
