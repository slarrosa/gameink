using UnityEngine;

public class CameraFollows : MonoBehaviour
{

    public Transform player;
    public Vector3 offset;
    [Range (1,10)]
    public float cameraDelay;


    void Update()
    {
        Vector3 playerPos = player.position + offset;   
        Vector3 smoothCamera = Vector3.Lerp(transform.position, playerPos, cameraDelay * Time.deltaTime);
        transform.position = new Vector3(smoothCamera.x,transform.position.y, playerPos.z);
        
    }
}
