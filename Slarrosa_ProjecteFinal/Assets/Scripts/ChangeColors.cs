using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColors : MonoBehaviour
{
    public void SetColor(Color color)
    {
        this.GetComponent<SpriteRenderer>().color = color;
    }
}
