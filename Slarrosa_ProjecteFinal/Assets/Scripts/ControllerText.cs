using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControllerText : MonoBehaviour
{
    public GameObject keyboardText;
    public GameObject gamepadText;

    // Update is called once per frame
    void Update()
    {
        var gamepad = Gamepad.current;
        if (gamepad == null)
        {
            keyboardText.gameObject.SetActive(true);
            gamepadText.gameObject.SetActive(false);
        }
        else
        {
            keyboardText.gameObject.SetActive(false);
            gamepadText.gameObject.SetActive(true);
        }
            
    }
}
