using UnityEngine;
using UnityEngine.SceneManagement;

public class DeadScript : MonoBehaviour
{
    public SO_Options settings;
    public void PlayAgain()
    {
        SceneManager.LoadScene(settings.SceneName);
    }

    public void Exit()
    {
        SceneManager.LoadScene("Lobby");
    }
}
