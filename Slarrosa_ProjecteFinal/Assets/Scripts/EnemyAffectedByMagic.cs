using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAffectedByMagic : MonoBehaviour
{
    public SO_EnemyStats stats;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "blueMagic")
        {
            StartCoroutine(disableEnemy());
            print(collision.name + " " + this.transform.tag);
        }
        else if (collision.transform.tag == "redMagic")
        {
            float forcePower = 3 * this.gameObject.GetComponent<Rigidbody2D>().mass;
            bool leftExplosion = false;
            if (collision.transform.position.x < transform.position.x)
                leftExplosion = true;

            this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(leftExplosion ? forcePower : -forcePower, forcePower*0.7f), ForceMode2D.Impulse);

        }
        else if (collision.transform.tag == "greenMagic")
        {
            StartCoroutine(getConfused());
        }

    }

    IEnumerator disableEnemy()
    {
        this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
        stats.disabled = true;
        stats.trueDisabled = true;
        yield return null;
    }
    IEnumerator getConfused()
    {
        Color normalColor = this.gameObject.GetComponent<SpriteRenderer>().color;
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        for (int i = 0; i < 5; i++)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(12, 0);
            yield return new WaitForSeconds(0.3f);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-12, 0);
            yield return new WaitForSeconds(0.3f);
        }
       
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

        this.gameObject.GetComponent<SpriteRenderer>().color = normalColor;
    }
}
