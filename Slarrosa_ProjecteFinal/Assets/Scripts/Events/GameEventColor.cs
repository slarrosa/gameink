using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Events/Color")]
public class GameEventColor : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GameEventListenerColor> eventListeners =
        new List<GameEventListenerColor>();

    public void Raise(Color color)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(color);
    }

    public void RegisterListener(GameEventListenerColor listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerColor listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
