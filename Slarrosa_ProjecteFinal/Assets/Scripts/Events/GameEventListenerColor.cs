using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerColor : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventColor Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<Color> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Color color)
    {
        Response.Invoke(color);
    }
}
