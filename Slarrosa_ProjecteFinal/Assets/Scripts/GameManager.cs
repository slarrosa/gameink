using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    GameEventColor color;

    public GameObject player;

    [SerializeField]
    private float colorChangeTime;

    public SO_Color myColor;
    public SO_Percs percs;
    public SO_Options settings;
    public GameObject ground;
    public GameObject groundnt;

    public PhysicsMaterial2D bouncyBoiMaterial;

    [SerializeField]
    GameEvent cooldownPocket;

    [SerializeField]
    private Color[] colors;

    public Image colorsCircle;

    public GameObject pocketCooldownImage;
    private int index = 0;
    private int indexNext = 1;

    private Vector3 playerNormalScale;
    private float playerNormalVelocity;

    public SO_PlayerStats playerstats;

    public AudioSource Music;

    private Color blueColor = new Color(0f, 0f, 1f, 1f);
    private Color greenColor = new Color(0f, 1f, 0f, 1f);
    private Color redColor = new Color(1f, 0f, 0f, 1f);
    private float timer = 0;
    private bool space;
    private int tick;

    private void Awake()
    {
        playerstats.moveSpeed = 15;
        playerstats.jumpForce = 17;
        playerNormalVelocity = playerstats.moveSpeed;
        playerNormalScale = player.transform.localScale;
        myColor.colorInPocket = Color.black;
        myColor.colorInPocketBlocked = false;
    }

    private void Start()
    {
        settings.SceneName = SceneManager.GetActiveScene().name;
        ColorChanger();
    }

    public void fusionColors(InputAction.CallbackContext context)
    {
        if (context.performed)
            space = true;
    }

    IEnumerator CooldownCircle() 
    {
        cooldownPocket?.Raise();
        yield return null;
        
    }

    void Update()
    {
        timer += Time.deltaTime;

        if(timer >= 5f)
        {
            timer -= 5f;
            tick++;
            ColorChanger();
        }
        colorsCircle.transform.eulerAngles = new Vector3(colorsCircle.transform.eulerAngles.x, colorsCircle.transform.eulerAngles.y, 120f * tick + (timer / 15f) * 360f);
    }
    void FixedUpdate()
    {
        if (myColor.actualColor == redColor && !myColor.colorCombined)
            ColorRed();
        if (myColor.actualColor == blueColor && !myColor.colorCombined)
            ColorBlue();
        if (myColor.actualColor == greenColor && !myColor.colorCombined)
            ColorGreen();
        if (percs.colorsPocketObtained)
        {
            if ((myColor.actualColor == redColor && myColor.colorInPocket == blueColor && space) || (myColor.actualColor == blueColor && myColor.colorInPocket == redColor && space))
            {
                print("aaa1");
                ColorMagenta();
                StartCoroutine(CooldownPocket());
                StartCoroutine(CooldownCircle());
            }
            if ((myColor.actualColor == greenColor && myColor.colorInPocket == blueColor && space) || (myColor.actualColor == blueColor && myColor.colorInPocket == greenColor && space))
            {
                print("aaa2");
                ColorCyan();
                StartCoroutine(CooldownPocket());
                StartCoroutine(CooldownCircle());
            }
            if ((myColor.actualColor == greenColor && myColor.colorInPocket == redColor && space) || (myColor.actualColor == redColor && myColor.colorInPocket == greenColor && space))
            {
                print("aaa3");
                ColorYellow();
                StartCoroutine(CooldownPocket());
                StartCoroutine(CooldownCircle());
            }
            
            space = false;       
        }
    }


    private void ColorChanger()
    {
        myColor.habilityUsedInColor = false;
        if (index >= colors.Length - 1)
            index = 0;
        else
            index++;
        if(indexNext >= colors.Length - 1)
            indexNext = 0;
        else
            indexNext++;
        myColor.actualColor = colors[index];
        print(myColor.actualColor);
        myColor.nextColor = colors[indexNext];
        color?.Raise(colors[index]);
        myColor.colorCombined = false;
        resetGravityAndStats();
        player.GetComponent<PlayerController>().CheckStucked();
    }

    void ColorRed()
    {
        if (Mathf.Sign(player.gameObject.GetComponent<Rigidbody2D>().gravityScale) == 1)
            player.gameObject.GetComponent<Rigidbody2D>().gravityScale *= -1;
        if(Mathf.Sign(player.gameObject.transform.localScale.y) == 1)
            player.gameObject.transform.localScale = new Vector3(player.gameObject.transform.localScale.x, player.gameObject.transform.localScale.y*-1, player.gameObject.transform.localScale.z);
    }

    void ColorBlue()
    {
        player.transform.localScale = new Vector3(0.25f,0.25f,0.25f);
        playerstats.moveSpeed = 20f;
        Music.pitch = 1.4f;
    }

    void ColorGreen()
    {
        
        ground.GetComponent<BoxCollider2D>().sharedMaterial = bouncyBoiMaterial;
        groundnt.GetComponent<BoxCollider2D>().sharedMaterial = bouncyBoiMaterial;

    }

    void ColorYellow()
    {
        myColor.colorCombined = true;
        color?.Raise(new Color(1f, 1f, 0f, 1f));
        myColor.colorInPocket = Color.black;
        ColorRed();
        ColorGreen();
    }

    void ColorCyan()
    {
        myColor.colorCombined = true;
        color?.Raise(new Color(0f, 1f, 1f, 1f));
        myColor.colorInPocket = Color.black;
        ColorBlue();
        ColorGreen();
    }

    void ColorMagenta()
    {
        myColor.colorCombined = true;
        color?.Raise(new Color(1f, 0f, 1f, 1f));
        myColor.colorInPocket = Color.black;
        ColorBlue();
        ColorRed();
    }

    void resetGravityAndStats()
    {
        if (Mathf.Sign(player.gameObject.GetComponent<Rigidbody2D>().gravityScale) == -1)
        {
            player.gameObject.GetComponent<Rigidbody2D>().gravityScale *= -1;
            player.gameObject.GetComponent<SpriteRenderer>().flipY = false;
        }
        player.transform.localScale = playerNormalScale;
        playerstats.moveSpeed = playerNormalVelocity;
        ground.GetComponent<BoxCollider2D>().sharedMaterial = null;
        groundnt.GetComponent<BoxCollider2D>().sharedMaterial = null;
        Music.pitch = 1f;
    }

    IEnumerator CooldownPocket()
    {
        yield return new WaitForSeconds(myColor.PocketCooldown);
        myColor.colorInPocketBlocked = false;
    }

}
