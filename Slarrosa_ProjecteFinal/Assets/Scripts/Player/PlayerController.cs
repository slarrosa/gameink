using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public SO_PlayerStats stats;
    public SO_Percs percs;
    public SO_Color colors;

    Animator animator;
    Animator animator2;
    private string currentState;

    public GameObject cursor;

    public GameObject blueMagic;
    public GameObject redMagic;
    public GameObject greenMagic;

    public float magicCursorVelocity;

    public SO_BulletInfo bulletInfo;

    //Estats del animator
    const string PLAYER_IDLE = "robotIdle";
    const string PLAYER_RUN = "robotRun";
    const string PHOTO_IDLE = "photoAnim";
    const string PHOTO_RUN = "photoanimrun";

    private Color blueColor = new Color(0f, 0f, 1f, 1f);
    private Color greenColor = new Color(0f, 1f, 0f, 1f);
    private Color redColor = new Color(1f, 0f, 0f, 1f);

    public GameObject panelWhenDead;
    public GameObject shield;
    private PlayerInputAction playerInputActions;
    private RaycastHit2D hit;
    private RaycastHit2D hit2;
    private bool OnADoor1;
    public GameObject panelEnd;
    public SO_Score score;
    public LayerMask wallLayer;

    public GameObject profilePic;

    public AudioSource explosion;
    void Awake()
    {
        panelEnd.SetActive(false);

        this.gameObject.SetActive(true);
        panelWhenDead.SetActive(false);
        stats.defense = true;
        //Inicialitzem l'animator
        animator = GetComponent<Animator>();
        animator2 = profilePic.GetComponent<Animator>();
        //Inicialitzem el Input System
        playerInputActions = new PlayerInputAction();
        //Habilitem el mapa d'accions de player
        playerInputActions.Player.Enable();
        //Subscribim el metode Jump a la accio Jump del mapa d'accions i fara que s'activi quan estigui en estat "performed" o sigui quan s'hagi apretat el boto
        playerInputActions.Player.Jump.performed += Jump;
    }
    private void Update()
    {
        bulletInfo.playerPos = this.transform.position;
        print("animacions: " + animator);
    }



    void FixedUpdate()
    {
        move();
        moveCursor();
    }

    public void CheckStucked()
    {
        //Fem un raycast en diagonal desde la esquerra del personatge que nomes pugui colisionar amb walls per la layer
        hit = Physics2D.Raycast(transform.position + new Vector3(-.48f, .33f, 0f), new Vector2(1f, 1f), 1.4f, wallLayer); // Left Rayo mcqueen
        Debug.DrawRay(transform.position + new Vector3(-.48f, .33f, 0f), new Vector2(1f, 1f).normalized * hit.distance, Color.magenta, 3f);

        // fem que si colisiona amb una wall el movem el personatge cap al costat i fem la funcio recursiva per que es mogui fins a que el raycast deixi de detectar res
        if (hit.collider != null)
        {
            bool isDeIsquierdas = transform.position.x < hit.collider.transform.position.x;
            transform.position += isDeIsquierdas ? Vector3.left * .1f : Vector3.right * .1f;
            
            CheckStucked();
            
            return;
        }
        // Fem el mateix que a datl pero amb un raycast sortint de la dreta del personatge
        hit2 = Physics2D.Raycast(transform.position + new Vector3(-.48f, .33f, 0f), new Vector2(-1f, 1f), 1.4f, wallLayer); // Right Rayo mcqueen

        if (hit2.collider != null)
        {
            bool isDeIsquierdas = transform.position.x < hit2.collider.transform.position.x;
            transform.position += isDeIsquierdas ? Vector3.left * .1f : Vector3.right * .1f;
            CheckStucked();
        }

    }
    public void Magic(InputAction.CallbackContext context)
    {
        
        if (context.canceled && !colors.habilityUsedInColor)
        {
            if(colors.actualColor == blueColor)
                StartCoroutine(activateMagic(blueMagic, "blueMagic"));
            else if (colors.actualColor == redColor)
                StartCoroutine(activateMagic(redMagic, "redMagic"));
            else if (colors.actualColor == greenColor)
                StartCoroutine(activateMagic(greenMagic, "greenMagic"));
        }
    }

    private void moveCursor()
    {

        float horizontalMovement = playerInputActions.Player.Magic.ReadValue<Vector2>().x * magicCursorVelocity;
        float verticalMovement = playerInputActions.Player.Magic.ReadValue<Vector2>().y * magicCursorVelocity;
        if ((horizontalMovement != 0 || verticalMovement != 0) && !colors.habilityUsedInColor)
        {
            cursor.gameObject.SetActive(true);
        }
        else
        {
            cursor.gameObject.SetActive(false);
            cursor.gameObject.transform.position = this.transform.position;
        }
            

        Vector2 newVelocity;
        newVelocity.x = horizontalMovement;
        newVelocity.y = verticalMovement;
        cursor.gameObject.GetComponent<Rigidbody2D>().velocity = newVelocity;
    }

    IEnumerator activateMagic(GameObject magic, string magicAnimationName)
    {
        colors.habilityUsedInColor = true;
        magic.transform.position = cursor.transform.position;
        magic.SetActive(true);
        magic.GetComponent<Animator>().Play(magicAnimationName);
        explosion.Play();
        yield return new WaitForSeconds(0.5f);
        magic.SetActive(false);    
    }

    public void move()
    {
        float horizontalMovement = playerInputActions.Player.Move.ReadValue<Vector2>().x * stats.moveSpeed;
        Vector2 newVelocity;
        newVelocity.x = horizontalMovement;
        newVelocity.y = this.gameObject.GetComponent<Rigidbody2D>().velocity.y;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = newVelocity;


        if (horizontalMovement < 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            ChangeAnimationState(PLAYER_RUN);
            ChangeAnimationState2(PHOTO_RUN);
            profilePic.transform.position = new Vector2(this.transform.position.x - 0.066f, profilePic.transform.position.y);
        }
        else if (horizontalMovement > 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            ChangeAnimationState(PLAYER_RUN);
            ChangeAnimationState2(PHOTO_RUN);
            profilePic.transform.position = new Vector2(this.transform.position.x + 0.066f, profilePic.transform.position.y);

        }
        if (horizontalMovement == 0)
        {
            ChangeAnimationState(PLAYER_IDLE);
            ChangeAnimationState2(PHOTO_IDLE);
        }

    }

    public void Retry(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            string escena = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(escena);
        }
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if (stats.jumpsAvailable > 0 && context.performed)
        {
            Vector2 newVelocity;
            newVelocity.x = this.GetComponent<Rigidbody2D>().velocity.x;
            if (Mathf.Sign(this.GetComponent<Rigidbody2D>().gravityScale) == 1)
                newVelocity.y = stats.jumpForce;
            else
                newVelocity.y = -stats.jumpForce;
            stats.jumpsAvailable--;
            this.GetComponent<Rigidbody2D>().velocity = newVelocity;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("ground") || collision.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            stats.jumpsAvailable = 2;
        }

        if(collision.transform.tag == "redMagic")
        {
            bool leftExplosion = false;
            if (collision.transform.position.x < transform.position.x)
                leftExplosion = true;

            this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(leftExplosion ? 10 : -10, -10f), ForceMode2D.Impulse);
        }

        if(collision.gameObject.tag == "final")
        {
            panelEnd.SetActive(true);
            score.scoreStop = true;
            stats.gameEnded = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.transform.tag == "DoorToWorld1")
        {
            OnADoor1 = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "DoorToWorld1")
        {
            OnADoor1 = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "bullet" && colors.actualColor== redColor)
        {
            this.gameObject.SetActive(false);
            panelWhenDead.SetActive(true);
        }else if (collision.transform.tag == "bullet" && stats.defense)
        {
            StartCoroutine(DefenseCooldown());
        }else if (collision.transform.tag == "bullet" && !stats.defense)
        {
            this.gameObject.SetActive(false);
            panelWhenDead.SetActive(true);
        }
    }

    public void enterWorld(InputAction.CallbackContext context)
    {
        if(OnADoor1 && context.performed)
        {
            SceneManager.LoadScene("Map1");
        }
    }

    IEnumerator DefenseCooldown()
    {
        
        for (int i = 0; i < 2; i++)
        {
            shield.SetActive(false);
            yield return new WaitForSecondsRealtime(0.2f);
            shield.SetActive(true);
            yield return new WaitForSecondsRealtime(0.2f);
            shield.SetActive(false);
        }
        yield return new WaitForSecondsRealtime(14.2f);
        shield.SetActive(true);
        stats.defense = true;
    }

    public void StoreColor(InputAction.CallbackContext context)
    {
        if (context.performed && colors.colorInPocket == Color.black && !colors.colorInPocketBlocked)
        {
            colors.colorInPocket = colors.actualColor;
            colors.colorInPocketBlocked = true;
        }
    }

    void ChangeAnimationState(string newState)
    {
        //Per que la animacio no s'interrompi a si mateixa
        if (currentState == newState)
            return;

        //activa la animacio
        animator.Play(newState);

        //reassignem el estat actual
        currentState = newState;
    }
    void ChangeAnimationState2(string newState)
    {
        //Per que la animacio no s'interrompi a si mateixa
        if (currentState == newState)
            return;

        //activa la animacio
        animator2.Play(newState);

        //reassignem el estat actual
        currentState = newState;
    }


}
