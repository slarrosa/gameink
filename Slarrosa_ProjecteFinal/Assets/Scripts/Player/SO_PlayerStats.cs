using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_PlayerStats : ScriptableObject
{
    public float moveSpeed;
    public float jumpForce;
    public float jumpsAvailable;
    public bool defense;
    public string id;
    public bool gameEnded;
}
