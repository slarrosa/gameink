using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PocketColorScript : MonoBehaviour
{
    public SO_Color color;
    public SO_Percs percs;
    void Start()
    {
        if (percs.colorsPocketObtained)
            this.gameObject.SetActive(true);
        else
            this.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (color.colorInPocket != Color.black)
        {
            this.gameObject.GetComponent<Image>().color = color.colorInPocket;
        }
        else
        {
            this.gameObject.GetComponent<Image>().color = Color.white;
        }
    }
}
