using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_BulletInfo : ScriptableObject
{
    public Vector3 playerPos;
    public float bulletVelocity;
}
