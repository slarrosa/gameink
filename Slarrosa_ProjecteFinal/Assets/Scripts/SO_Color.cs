using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Color")]
public class SO_Color : ScriptableObject
{
    public Color actualColor;
    public Color nextColor;
    public Color colorInPocket;
    public bool colorInPocketBlocked;
    public bool habilityUsedInColor;
    public float PocketCooldown;
    public bool colorCombined;
}
