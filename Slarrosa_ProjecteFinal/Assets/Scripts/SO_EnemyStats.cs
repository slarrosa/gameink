using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_EnemyStats : ScriptableObject
{
    public float moveSpeed;
    public bool activated;
    public bool disabled;
    public bool trueDisabled;
    public float reloadSpeed;
}
