using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class SO_FaceUrl : ScriptableObject
{
    public Texture2D face;
}
