using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_Options : ScriptableObject
{
    public float RightStickSensibility;
    public string SceneName;
}
