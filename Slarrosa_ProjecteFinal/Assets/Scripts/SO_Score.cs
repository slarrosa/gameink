using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_Score : ScriptableObject
{
    public float score;
    public bool scoreStop;
}
