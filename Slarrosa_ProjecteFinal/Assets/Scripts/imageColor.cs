using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class imageColor : MonoBehaviour
{

    public SO_Color color;


    void Update()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = color.nextColor;
    }
}
