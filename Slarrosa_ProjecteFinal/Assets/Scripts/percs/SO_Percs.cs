
using UnityEngine;
[CreateAssetMenu]
public class SO_Percs : ScriptableObject
{
    public bool doubleJumpObtained;
    public bool colorsPocketObtained;
}
