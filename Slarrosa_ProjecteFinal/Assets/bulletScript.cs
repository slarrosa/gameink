using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    public SO_BulletInfo bulletInfo;
    public SO_Color colors;
    private Color blueColor = new Color(0f, 0f, 1f, 1f);
    private Color greenColor = new Color(0f, 1f, 0f, 1f);
    private Color redColor = new Color(1f, 0f, 0f, 1f);

    private bool flag = false;
    Color colorOnSpawn;
    private void Start()
    {
        colorOnSpawn = colors.actualColor;
        this.GetComponent<SpriteRenderer>().color = colorOnSpawn;
    }

    void Update()
    {
        if (colorOnSpawn == greenColor)
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = (bulletInfo.playerPos - transform.position).normalized * bulletInfo.bulletVelocity;
        }
        else if (colorOnSpawn == blueColor)
        {
            if(!flag)
                StartCoroutine(blueChanger());
        }else if (colorOnSpawn == redColor)
        {
            if (!flag)
                StartCoroutine(redChanger());
        }
        
        
    }

    IEnumerator redChanger()
    {
        flag = true;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = (bulletInfo.playerPos - transform.position).normalized * bulletInfo.bulletVelocity;
        yield return null;
    }
    IEnumerator blueChanger()
    {
        flag = true;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = (bulletInfo.playerPos - transform.position).normalized * (bulletInfo.bulletVelocity*3);
        yield return null;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
    }
}
