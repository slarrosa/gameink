using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menuManager : MonoBehaviour
{
    public GameObject panel;
    public GameObject emailField;
    public GameObject passwordField;
    public GameObject errorMessage;
    private string myId;
    public SO_FaceUrl faceUrl;
    public SO_PlayerStats player;

    void Start()
    {
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        passwordField.GetComponent<TMPro.TMP_InputField>().contentType = TMPro.TMP_InputField.ContentType.Password;
    }

    public void loginButton()
    {
        StartCoroutine(getId());
    }
    IEnumerator getId()
    {
        string email = emailField.GetComponent<TMPro.TMP_InputField>().text;
        string pwd = passwordField.GetComponent<TMPro.TMP_InputField>().text;
        string uri = "http://127.0.0.1:8000/api/getId/" + email + "/" + pwd;

        using (UnityWebRequest request = UnityWebRequest.Get(uri))
        {
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                errorMessage.GetComponent<TMPro.TextMeshProUGUI>().text = request.error;
            }
            else
            {
                errorMessage.GetComponent<TMPro.TextMeshProUGUI>().text = "Login succesfully";
                myId = request.downloadHandler.text;
                player.id = myId;
                StartCoroutine(loadImage());
            }
        }
    }

    IEnumerator loadImage()
    {
        string uri2 = "http://127.0.0.1:8000/api/getPfp/";
        uri2 += myId;
        WWW www = new WWW(uri2);
        yield return www;
        if(www.error == null)
        {
            Texture2D texture = www.texture;
            faceUrl.face = texture;
            //image.texture = faceUrl.face;
        }
        else
        {
            Debug.Log("Error getteing the image");
        }
    }

    public void startGame()
    {
        SceneManager.LoadScene("Lobby");
    }

    public void ClosePanel()
    {
        StartCoroutine(closePanelCorroutine());
    }

    public void OpenPanel()
    {
        panel.SetActive(true);
    }
    IEnumerator closePanelCorroutine()
    {
        yield return new WaitForSecondsRealtime(3f);
        panel.SetActive(false);
    }
}
