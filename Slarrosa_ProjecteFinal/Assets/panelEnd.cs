using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class panelEnd : MonoBehaviour
{
    public SO_Score score;
    public GameObject textScore;
    public SO_PlayerStats player;
    private bool flag;
    void Start()
    {
        flag = false;
    }

    // Update is called once per frame
    void Update()
    {
        textScore.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Your score is: " + score.score;

        //setScore /{ id}/{ newscore}
        if (player.gameEnded && player.id != null && !flag)
        {
            StartCoroutine(sendScore());
            flag = true;
        }
    }

    IEnumerator sendScore()
    {
        string uri = "http://127.0.0.1:8000/api/setScore/" + player.id + "/" + score.score;
        using (UnityWebRequest request = UnityWebRequest.Get(uri))
        {
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                Debug.Log("Data sended");
            }
        }
    }
}
